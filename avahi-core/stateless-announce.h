#ifndef foostatelessannouncehfoo
#define foostatelessannouncehfoo

void avahi_stateless_announce_entry(AvahiServer* s, AvahiEntry* e);
void avahi_stateless_reannounce_entry(AvahiServer *s, AvahiEntry *e);
void avahi_stateless_goodbye_entry(AvahiServer *s, AvahiEntry *e, int send_goodbye, int remove);


void avahi_stateless_announce_group(AvahiServer *s, AvahiSEntryGroup *g);
void avahi_stateless_check_group_probed(AvahiSEntryGroup *g, int immediately);

#endif
